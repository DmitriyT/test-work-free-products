<?php
/**
 * Plugin Name:       Woocommerce Test Work Plugin
 * Plugin URI:#
 * Description: Woocommerce Test Work Plugin.
 * Version:           1.0.0
 * Author:            Dmitriy Tatanov
 * Author URI:        #
 * Text Domain:     woo-test-work
 *
 * @since             1.0.0
 *
 * @wordpress-plugin
 */

define( 'WOO_TW_TEXTDOMAIN', 'woo-test-work' );
define('WOO_DIR_PATH', plugin_dir_path(__FILE__));
define('WOO_DIR_URI', plugin_dir_url(__FILE__));
define('WOO_PLUGIN_VERSION', '1.0.0');

require_once WOO_DIR_PATH . '/includes/class-woo-test-work.php';
require_once WOO_DIR_PATH . '/includes/class-woo-test-work-helper.php';
require_once WOO_DIR_PATH . '/includes/views/class-render-view.php';
require_once WOO_DIR_PATH . '/includes/admin/class-admin-settings.php';
require_once WOO_DIR_PATH . '/includes/class-woo-test-work-add-product.php';

/**
 * Show notice if Woocommerce plugin is not active.
 *
 * @return void
 */
function check_if_woocommerce_plugin_is_active() {

	if ( ! function_exists( 'WC' ) ) {
		// show admin notice on error.
		add_action(
			'admin_notices',
			function () {
				$class   = 'notice notice-error is-dismissible';
				$message = __( 'Please install and activate `Woocommerce` plugin ', WOO_TW_TEXTDOMAIN );

				printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) );
			}
		);
	}

}

register_activation_hook( __FILE__, 'check_if_woocommerce_plugin_is_active');