<?php

namespace Woo_Test_Work\Helper;

class Woo_Test_Work_Helper {

	public static function need_free_products(){

		$need = false;
		$trigger_cat = get_option('tw_category_for_scale');
		$trigger_count = get_option('tw_products_number_for_scale');
		$products_count = 0;

		foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item){

			$product = $cart_item['data'];

			if(!is_object($product)){
				continue;
			}

			if($product->is_type('variation')){
				$product = wc_get_product($product->get_parent_id());
			}

			$product_categories = $product->get_category_ids();

			if(empty($product_categories) || !is_array($product_categories)){
				continue;
			}

			if(in_array($trigger_cat, $product_categories)){
				$products_count = $products_count + $cart_item['quantity'];
			}

		}

		if($products_count >= $trigger_count){
			$need = true;
		}

		return $need;

	}

	public static function product_already_in_cart($return_id = false){

		$in_cart = false;
		$product_id = null;

		foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item){

			$product = $cart_item['data'];

			if(!is_object($product)){
				continue;
			}

			$price = WC()->cart->get_product_price( $product );

			if($price == 0){
				$free_products_category = get_option( 'tw_category_free_products' );

				if($product->is_type('variation')){
					$parent_product = wc_get_product($product->get_parent_id());
					$cat_ids = $parent_product->get_category_ids();
				}else{
					$cat_ids = $product->get_category_ids();
				}

				if(in_array($free_products_category, $cat_ids)){
					$in_cart = true;
					$product_id = $product->get_id();
					break;
				}

			}

		}

		return ($return_id) ? $product_id : $in_cart;

	}

	public static function remove_from_cart($product_id) {

		foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item){

			if($product_id == $cart_item['data']->get_id()){
				WC()->cart->remove_cart_item( $cart_item_key );
			}

		}

	}

}