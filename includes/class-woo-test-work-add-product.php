<?php

namespace Woo_Test_Work\Add_Product;

use Woo_Test_Work\Helper\Woo_Test_Work_Helper;

class Woo_Test_Work_Add_Product {

	public function __construct() {
		add_action( 'woocommerce_update_cart_action_cart_updated', [$this, 'setup_free_product_in_cart'] );
		add_action( 'woocommerce_before_calculate_totals', [$this, 'setup_free_product_price'] );
		add_action( 'woocommerce_cart_item_removed', [$this, 'remove_free_product'], 10 );
	}

	public function remove_free_product(){

		if(!Woo_Test_Work_Helper::need_free_products()){

			if(Woo_Test_Work_Helper::product_already_in_cart()){

				$free_product_id = Woo_Test_Work_Helper::product_already_in_cart(true);

				if($free_product_id){
					Woo_Test_Work_Helper::remove_from_cart($free_product_id);
				}

			}

		}

	}

	/**
	 * @param $cart_object
	 * Setup free price for product
	 */
	public function setup_free_product_price( $cart_object ){

		foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item){

			if(array_key_exists( 'free_product', $cart_item ) &&
				$cart_item['free_product'] === true){

				$cart_item['data']->set_price(0);

			}

		}

	}

	/**
	 * @return bool|string
	 * @throws \Exception
	 * Add or Remove free product from cart
	 */
	public function setup_free_product_in_cart(){

		if(!Woo_Test_Work_Helper::need_free_products()){

			if(Woo_Test_Work_Helper::product_already_in_cart()){

				$free_product_id = Woo_Test_Work_Helper::product_already_in_cart(true);

				if($free_product_id){
					Woo_Test_Work_Helper::remove_from_cart($free_product_id);
				}

			}

		}

		if(isset($_REQUEST['free_product_id']) && !empty($_REQUEST['free_product_id'])){

			if(!Woo_Test_Work_Helper::product_already_in_cart()){

				$add_to_cart = WC()->cart->add_to_cart((int) $_REQUEST['free_product_id'], 1, 0, [], [
					'free_product' => true
				]);

				return $add_to_cart;

			}

		}

		return false;

	}

}

new Woo_Test_Work_Add_Product();