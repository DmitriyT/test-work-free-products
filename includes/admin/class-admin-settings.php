<?php

namespace Woo_Test_Work\Admin;

use Woo_Test_Work\Views\Render_View;

class Admin_Settings {

	public function __construct() {
		add_action( 'admin_menu', [ $this, 'woo_test_work_admin_page' ] );
		add_action( 'admin_init', [ $this, 'woo_test_work_admin_page_settings' ] );
	}

	/**
	 * Register Settings Group and Settings Fields
	 */
	public function woo_test_work_admin_page_settings() {
		add_settings_section( 'woo_tw_settings',
		                      esc_html__( 'Woo Test Work', WOO_TW_TEXTDOMAIN ),
		                      [ $this, 'render_admin_page_settings' ],
		                      'woo-test-work' );

		register_setting( 'woo_tw_settings', 'tw_category_for_scale' );
		register_setting( 'woo_tw_settings', 'tw_products_number_for_scale' );
		register_setting( 'woo_tw_settings', 'tw_category_free_products' );
	}

	/**
	 * Register Admin Page for Plugin Settings
	 */
	public function woo_test_work_admin_page() {
		add_menu_page( 'Woo Test Work',
		               'Woo Test Work',
		               'manage_options',
		               'woo-test-work',
		               [
			               $this,
			               'render_admin_page'
		               ] );
	}

	public function render_admin_page() {
		Render_View::view( 'admin_page' );
	}

	public function render_admin_page_settings() {
		Render_View::view( 'admin_page_settings' );
	}

}

new Admin_Settings();