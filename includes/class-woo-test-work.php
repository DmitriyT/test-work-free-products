<?php

namespace Woo_Test_Work;

class Woo_Test_Work {

	public function __construct() {

		add_action( 'wp_enqueue_scripts', [$this, 'woo_test_work_styles'], 10);
		add_action( 'admin_enqueue_scripts', [$this, 'enqueuing_admin_scripts'] );

	}

	/**
	 * @param $hook
	 * Enqueue admin scripts & styles
	 */
	public function enqueuing_admin_scripts($hook){
		global $pagenow;

		if($hook === 'toplevel_page_woo-test-work'){
			wp_enqueue_style('admin-woo-test-work-css', WOO_DIR_URI . '/assets/admin/woo-test-work.css');
			wp_enqueue_script('admin-woo-test-work-js', WOO_DIR_URI . '/assets/admin/woo-test-work.js');
		}

	}

	/**
	 * Enqueue front scripts & styles
	 */
	public function woo_test_work_styles(){
		wp_enqueue_style( 'woo-test-work-css', WOO_DIR_URI . '/assets/woo-test-work.css', [], WOO_PLUGIN_VERSION );
		wp_enqueue_script( 'woo-test-work-js', WOO_DIR_URI . '/assets/woo-test-work.js', [], WOO_PLUGIN_VERSION, true );

	}

}

new Woo_Test_Work();