<?php

add_action( 'woocommerce_cart_contents', 'render_cart_line_free_product', 15 );

function render_cart_line_free_product() {

    if(!\Woo_Test_Work\Helper\Woo_Test_Work_Helper::need_free_products() ||
       \Woo_Test_Work\Helper\Woo_Test_Work_Helper::product_already_in_cart()){
        return false;
    }

    if(isset($_REQUEST['free_product_id']) &&
        \Woo_Test_Work\Helper\Woo_Test_Work_Helper::product_already_in_cart($_REQUEST['free_product_id'])){
        return false;
    }

	$free_products_category = get_option( 'tw_category_free_products' );

	if ( empty( $free_products_category ) || $free_products_category == 0 ) {
		return false;
	}
	?>
    <div class="free_product_line">
        <div><span><?php
				esc_html_e( 'Select Free Product:', WOO_TW_TEXTDOMAIN ); ?></span></div>
        <div><?php
			\Woo_Test_Work\Views\Render_View::view( 'select_products', $free_products_category); ?></div>
    </div>
	<?php
}