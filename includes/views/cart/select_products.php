<?php

add_action( 'select_products', 'render_select_products' );

function render_select_products($cat_id) {

	if(empty($cat_id)){
		return false;
	}

	$args = array(
		'posts_per_page' => -1,
		'orderby' => 'date',
		'order' => 'DESC',
		'post_type' => 'product',
		'tax_query' => [
			[
				'taxonomy' => 'product_cat',
				'fields' => 'term_id',
				'terms' => $cat_id
			]
		]
	);

	$query = new WP_Query( $args );

	if ( $query->have_posts() ) {

		echo '<select name="free_product_id">';

		echo '<option value="" selected disabled hidden>'.esc_html__('Select Product', WOO_TW_TEXTDOMAIN).'</option>';

		while ( $query->have_posts() ) {
			$query->the_post();
			echo '<option value="'.get_the_ID().'">' . get_the_title() . '</option>';
		}

		echo '</select>';

	}else{
		echo 'empty';
	}

	wp_reset_postdata();

}