<?php

add_action( 'admin_page_settings', 'render_admin_page_settings' );

function render_admin_page_settings() {
?>
    <div class="settings_box">
        <div class="settings_box_list">

            <div class="settings_box_item">
                <?php \Woo_Test_Work\Views\Render_View::view('categories_select', 'tw_category_for_scale', esc_html__('What category is the discount for:', WOO_TW_TEXTDOMAIN)); ?>
            </div>

            <div class="settings_box_item">
                <?php \Woo_Test_Work\Views\Render_View::view('input_of_number', 'tw_products_number_for_scale', esc_html__('How many items are needed for a discount:', WOO_TW_TEXTDOMAIN)); ?>
            </div>

            <div class="settings_box_item">
                <?php \Woo_Test_Work\Views\Render_View::view('categories_select', 'tw_category_free_products', esc_html__('Category for goods as a gift:', WOO_TW_TEXTDOMAIN)); ?>
            </div>

        </div>
    </div>
<?php
}