<?php
add_action( 'admin_page', 'render_admin_page' );

function render_admin_page() {
	?>

	<div class="woo-test-work-wrapper">
		<div class="container">
			<div class="page_title">
				<h1><?php echo esc_html__('Woocommerce Test Work Plugin Settings'); ?></h1>
			</div>
		</div>

		<div class="container">
            <form method="POST" action="options.php" class="woo_tw_settings_form">
	            <?php
	            settings_fields( 'woo_tw_settings' );
	            do_settings_sections( 'woo-test-work' );
	            submit_button();
	            ?>
            </form>
		</div>

	</div>


	<?php
}