<?php

add_action( 'categories_select', 'render_categories_select', 10, 2);

function render_categories_select($select_name, $title = '') {

	$selected_category = get_option($select_name);

	$product_cats = get_terms('product_cat', [
		'hide_empty' => false,
		'fields' => 'id=>name',
		'exclude' => [1]
	]);

	if(!is_array($product_cats) || empty($product_cats)){
		return false;
	}

	echo '<div class="product_cats_select">';

		if(!empty($title)){
			echo '<div class="select_title">' . esc_html($title) . '</div>';
		}

		echo '<select id="'.esc_attr($select_name).'" name="'.esc_attr($select_name).'">';

			foreach($product_cats as $cat_id => $cat_name){
				echo '<option '.selected($selected_category, $cat_id).' value="'.esc_attr($cat_id).'">'.esc_html($cat_name).'</option>';
			}

		echo '<select>';

	echo '</div>';

}