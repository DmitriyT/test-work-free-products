<?php

add_action( 'input_of_number', 'render_input_of_number', 10, 2);

function render_input_of_number($input_name, $title = '') {

	echo '<div class="input_of_number">';

	if(!empty($title)){
		echo '<div class="input_of_number_title">' . esc_html($title) . '</div>';
	}

	echo '<input type="number" value="'.esc_attr(get_option($input_name)).'" class="input_of_number_input" name="'.$input_name.'">';

	echo '</div>';

}