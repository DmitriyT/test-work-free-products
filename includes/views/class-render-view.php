<?php

namespace Woo_Test_Work\Views;

class Render_View {

	public $view_path = WOO_DIR_PATH . 'includes/views/';

	public function __construct() {

		require $this->view_path . 'admin/admin_page.php';
		require $this->view_path . 'admin/admin_page_settings.php';
		require $this->view_path . 'admin/elements/categories_select.php';
		require $this->view_path . 'admin/elements/input_of_number.php';

		require $this->view_path . 'cart/cart_line_free_product.php';
		require $this->view_path . 'cart/select_products.php';

	}

	public static function view($action, ...$args){
		do_action($action, ...$args);
	}

}

new Render_View();